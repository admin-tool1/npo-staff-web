import React, { Component } from "react";
import {
  CButton,
  CRow,
  CCol,
  CFormGroup,
  CInput,
  CInputGroup,
  CLabel,
  CCard,
  CCardHeader,
  CCardBody,
  CCardFooter,
} from "@coreui/react";
import api from "src/services/api";
import RequestAssets from "./RequestAssetForm";

class AssetPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      asset_id: "",
      showDetails: false,
      showForm: false,
      item_id: "",
      asset_name: "",
      asset_prefix: "",
      item_number: "",
      item_code: "",
      item_status_id: "",
      item_status: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.getAsset = this.getAsset.bind(this);
    this.getForm = this.getForm.bind(this);
  }

  getAsset() {
    api.get("/api/assetItem/" + this.state.item_id).then((res) => {
      this.setState({
        asset_id: res.data.asset_id,
        item_number: res.data.item_number,
        item_status_id: res.data.status_id,
      });
    });

    api.get("/api/getAsset/" + this.state.asset_id).then((res) => {
      var assets = res.data;
      assets.forEach((asset) => {
        if (asset.id === this.state.asset_id) {
          this.setState({
            asset_name: asset.asset_name,
            asset_prefix: asset.asset_prefix,
          });
        }
      });
      this.setState({
        item_code:
          this.state.asset_prefix +
          ("00000" + this.state.item_number).slice(-3),
      });
    });

    api
      .get("/api/getStatus/" + this.state.item_status_id)
      .then((res) => {
        var statuses = res.data;
        statuses.forEach((status) => {
          if (status.id === this.state.item_status_id) {
            this.setState({ item_status: status.status });
          }
        });
      })
      .then(() => {
        this.setState({ showDetails: true });
      });
  }

  getForm() {
    this.setState({ showForm: true });
  }

  handleNewId(){
    window.location.reload();
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    const { showDetails, showForm } = this.state;
    return (
      <div>
        <CRow className="justify-content-center">
          <CCol xl={6}>
            <CFormGroup>
              <CInputGroup>
                <CCol sm="3">
                  <CLabel>Item ID</CLabel>
                </CCol>
                <CCol>
                  <CInput
                    name="item_id"
                    value={this.state.item_id}
                    onChange={this.handleChange}
                    disabled={showDetails ? true : false}
                  />
                </CCol>
              </CInputGroup>
            </CFormGroup>
          </CCol>
        </CRow>
        <CRow className="justify-content-center">
          <CButton
            color="info"
            onClick={this.getAsset}
            disabled={showDetails ? true : false}
          >
            Get Item
          </CButton>
        </CRow>

        {showDetails && (
          <div>
            <CRow className="justify-content-center">
              <CCol xl={6}>
                <CCard className="m-3 p-3">
                  <CCardHeader>
                    <h4 className="text-center">Item Details</h4>
                  </CCardHeader>
                  <CCardBody>
                    <CRow>
                      <CCol sm="3" className="m-1">
                        <CLabel>Name</CLabel>
                      </CCol>
                      <CCol className="p-1">
                        <CInput
                          className="text-center form-control-static"
                          name="asset_name"
                          readOnly
                          value={this.state.asset_name}
                        />
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol sm="3" className="m-1">
                        <CLabel>Code</CLabel>
                      </CCol>
                      <CCol className="p-1">
                        <CInput
                          className="text-center form-control-static"
                          name="item_code"
                          readOnly
                          value={this.state.item_code}
                        />
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol sm="3" className="m-1">
                        <CLabel>Availability</CLabel>
                      </CCol>
                      <CCol className="p-1">
                        <CInput
                          className="text-center form-control-static"
                          name="item_status"
                          readOnly
                          value={this.state.item_status}
                        />
                      </CCol>
                    </CRow>
                  </CCardBody>
                  <CCardFooter className="text-center">
                    <CButton
                      color={
                        this.state.item_status === "Available" ? "info" : "light"
                      }
                      // to={{
                      //   pathname: "/request-asset",
                      //   state: { item_id: this.state.item_id },
                      // }}
                      onClick={this.getForm}
                      disabled={
                        this.state.item_status === "Available" ? false : true
                      }
                    >
                      {this.state.item_status === "Available"
                        ? "Go to Form"
                        : "Unavailable"}
                    </CButton> &nbsp;
                    <CButton
                      color="info"
                      onClick={this.handleNewId}
                      // disabled={
                      //   this.state.item_status === "Available" ? false : true
                      // }
                    >
                      New Item
                    </CButton>
                  </CCardFooter>
                </CCard>
              </CCol>
            </CRow>
          </div>
        )}
        {showForm && (
          <div>
            <CRow className="justify-content-center m-3 p-3">
              <CCol xl={6}>
                <RequestAssets item_id={this.state.item_id} />
              </CCol>
            </CRow>
          </div>
        )}
      </div>
    );
  }
}

export default AssetPage;
