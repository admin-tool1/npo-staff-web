import { React, Component } from "react";
import { CLink } from "@coreui/react";
import api from "src/services/api";

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 1,
    }}
  />
);

const LinkStyle = {
  textDecoration: "none",
  color: "#768192",
  fontSize: 20,
  fontWeight: 600,
};

class TheLink extends Component {
  constructor() {
    super();
    this.state = {
      hover: false,
    };

    this.toggleHover = this.toggleHover.bind(this);
    this.toggleOutHover = this.toggleOutHover.bind(this);
  }

  toggleHover(e) {
    e.target.style.color = "#3c4b64";
  }

  toggleOutHover(e) {
    e.target.style.color = "#768192";
  }

  handleLogout() {
    api.post("/api/logout").then((res) => {
      localStorage.clear();
      window.location.reload();
    });
  }

  render() {
    return (
      <div>
      <CLink
          to="confirm-asset"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
        >
          1. Scan and Confirm Asset
        </CLink>
        <ColoredLine color="info" />
        <CLink
          to="request-asset"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
        >
          2. Asset Request Form
        </CLink>
        <ColoredLine color="info" />
        <CLink
          to="return-asset"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
        >
          3. Asset Returned Declaration
        </CLink>
        <ColoredLine color="info" />
        <CLink
          to="/"
          style={LinkStyle}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleOutHover}
          onClick={this.handleLogout.bind(this)}
        >
          Logout
        </CLink>
      </div>
    );
  }
}
export default TheLink;
