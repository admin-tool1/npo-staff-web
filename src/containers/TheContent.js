import React, { Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { CContainer, CFade, CCol, CRow } from "@coreui/react";
// import Breadcrumbs from "./TheBreadcrumb.js";
// import Link from "./TheLink.js";

// routes config
import routes from "../routes";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const TheContent = () => {
  return (
    <main className="c-main">
      <CContainer fluid>
        <CRow className="justify-content-center mb-5" md={{ gutterX: 5 }}>
          {/* <CCol md={3}>
            <Link />
          </CCol> */}
          <CCol>
            {/* <Breadcrumbs /> */}
            <Suspense fallback={loading}>
              <Switch>
                {routes.map((route, idx) => {
                  return (
                    route.component && (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={(props) => (
                          <CFade>
                            <route.component {...props} />
                          </CFade>
                        )}
                      />
                    )
                  );
                })}
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </Suspense>
          </CCol>
        </CRow>
      </CContainer>
    </main>
  );
};

export default React.memo(TheContent);
